# ALBY-
![mona-whisper](https://user-images.githubusercontent.com/64751167/95435221-f0266500-096f-11eb-8070-57f6721b1857.gif)

<h1 align="center">Hi, I'm <a href="https://www.instagram.com/i_am_albin_praveen/">ALBY </a>!</h1>
<h1 align="center">Welcome to my GitHub profile</h1> 
<p align="center">   <a href="https://github.com/ALBINPRAVEEN"><img src="https://github-readme-stats.vercel.app/api?username=ALBINPRAVEEN&show_icons=true&include_all_commits=true&theme=chartreuse-dark&cache_seconds=3200" alt="ALBY'S GITHUB stats"></a>
 


<p align="center">   <a href="https://github.com/ALBINPRAVEEN"><img src="https://github-profile-trophy.vercel.app/?username=ALBINPRAVEEN&row=1" </a>
 
 <p align="center">   <a href="https://github.com/ALBINPRAVEEN"><img src="https://github-readme-stats.vercel.app/api/top-langs/?username=ALBINPRAVEEN&theme=blue-green" </a>
 
  <p align="center">   <a href="https://github.com/ALBINPRAVEEN"><img src="https://github-readme-streak-stats.herokuapp.com/?user=ALBINPRAVEEN&theme=blue-green" </a>
 
<p align="center">   <strong><a href="https://albinpraveen.github.io/">Official Website</a></strong> 

![M2TsZIT](https://user-images.githubusercontent.com/64751167/91557308-e1509980-e951-11ea-9b57-695796bd82cf.gif)
</p> 

<p align="center">❤ I'm currently [will be updated]</p> 

</br>
</br>
</br>


# About ME 💬 :

### - I'm 16 years  old Student from KERALA(India).Still studying.Future goal to be a CEH 
<p align="center">
<a href="https://www.instagram.com/i_am_albin_praveen/"><img title="Instagram" src="https://img.shields.io/badge/i_am_albin_praveen-black?style=for-the-badge&logo=instagram"></a>
<a href="mailto:albinpraveen135790@gmail.com"><img title="MAIL" src="https://img.shields.io/badge/ALBY-black?style=for-the-badge&logo=Gmail"></a>
</p>
<p align="center">
<a href="https://t.me/i_am_albin_praveen"><img title="Telegram" src="https://img.shields.io/badge/i_am_albin_praveen-black?style=for-the-badge&logo=telegram"></a>
<a href="https://wa.me/+917025743032"><img title="ALBY" src="https://img.shields.io/badge/ALBY-black?style=for-the-badge&logo=Whatsapp"></a>
</p>
<p align="center">
<a href="https://github.com/ALBINPRAVEEN"><img title="Github" src="https://img.shields.io/badge/ALBIN PRAVEEN-black?style=for-the-badge&logo=github"></a>
 </p>


<img hight="400" width="500" alt="GIF" align="right" src="https://github.com/Xx-Ashutosh-xX/Xx-Ashutosh-xX/blob/master/assets/1936.gif">



### - Learning :
- ✨ CERTIFIED ETHICAL HACKING
- ✨ HACKING SKILLS

### - Hobbies : 
- ✨ Djing
- ✨ MUSIC
- ✨ HACKING

### - FUTURE GOAL : 
- ✨ CERTIFIED ETHICAL HACKER
</br>
</br>
</br>

# Well known OS 👨‍💻 🛠:
</br> 
<p align="center"> 

<img src="https://img.shields.io/badge/KALI_LINUX-black?style=for-the-badge&logo=kali_linux" alt="Kali Linux" width="120" hight="50">
<img src="https://img.shields.io/badge/WINDOWS-black?style=for-the-badge&logo=windows" alt="Windows"  width="120" hight="50">
<img src="https://img.shields.io/badge/Android-black?style=for-the-badge&logo=android" alt="Android" width="120" hight="50">
</p>
</p>
</br>
</br>
</br>


# Languages👨‍💻 🛠:
</br> 
<p align="center">
<img src="https://img.shields.io/badge/PYTHON-black?style=for-the-badge&logo=python" alt="Python" width="120" hight="50">
<img src="https://img.shields.io/badge/Html-black?style=for-the-badge&logo=html" alt="HTML" width="120" hight="50">

</p>
</br>
</br>
</br>





# Contact Me :

<p>
 </br>





If you want to reach out to me about anything, be it some doubt or just to hangout and talk or want to game together just ping me 😉.

<a href="mailto:albinpraveen135790@gmail.com">
 <img align="left" alt="Gmail" width="130" hight="100" src="https://img.shields.io/badge/Gmail-black?style=for-the-badge&logo=Gmail">
</a>
<a href="https://www.instagram.com/i_am_albin_praveen/">
  <img align="left" alt="Instagram" width="130" hight="100" src="https://img.shields.io/badge/INSTAGRAM-black?style=for-the-badge&logo=instagram" />

</a>
<a href="https://wa.me/+917025743032">
  <img align="left" alt=" Whatsapp" width="130" hight="100" src="https://img.shields.io/badge/whatsapp-black?style=for-the-badge&logo=whatsapp">
</a>
<a href="https://github.com/ALBINPRAVEEN">
  <img align="left" alt="Github" width="130" hight="100" src="https://img.shields.io/badge/GITHUB-black?style=for-the-badge&logo=github" />
</a>
<a href="https://t.me/i_am_albin_praveen">
 <img align="left" alt="Telegram" width="130" hight="100" src="https://img.shields.io/badge/Telegram-black?style=for-the-badge&logo=telegram" />
</a>
 </p>
 

</br>
</br>
</br>
</br>
</br>
</br>
</br>


<!--
